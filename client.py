import time
import copy
from util import Constants as C

from util import Logger
from pymetasploit3.msfrpc import MsfRpcClient

from requests.exceptions import ConnectionError


class MetasploitWrapper:
    """
    Abstraction layer for Metasploit Client interactions
    """
    # Delay in seconds before checking for open sessions
    GET_SESSIONS_DELAY = 1
    READ_CONSOLE_DELAY = 1
    READ_CONSOLE_BUSY_ATTEMPTS = 5

    def __init__(self, password, port=55553, server="0.0.0.0", ssl=True):
        try:
            self.client = MsfRpcClient(password, port=port, server=server, ssl=ssl)
        except ConnectionError as e:
            print(f"{C.COL_RED}[-] Can't connect to msf rpc server @{server}:{port} with password: {password}{C.COL_RESET}")
            Logger.log(self, f"RPC client connection error - {port=}, {server=}, {ssl=}", level=Logger.ERROR)
            exit(1)

        Logger.log(self, f"RPC client connected - {port=}, {server=}, {ssl=}", level=Logger.INFO)

        self.cid = self.client.consoles.console().cid
        Logger.log(self, f"Console created - {self.cid=}", level=Logger.INFO)

        # flush the banner
        self.client.consoles.console(self.cid).read()

    def use_module(self, m_type, m_path):
        """
        Equivalent to "use module_path" on a msfconsole
        Instead of specifying the whole path we need to specify
        separately the module type and the path.

        i.e.
            use_module("exploit", "unix/misc/exploit_file")

        """
        Logger.log(self, f"Using module - {m_type=}, {m_path=}", level=Logger.INFO)
        return self.client.modules.use(m_type, m_path)

    def get_module_options(self, module, missing_only=True):
        """
        Returns module options to set.
        Specifying missing_only=True makes the function return
        parameters that still need to be set only.
        """
        if missing_only:
            return module.missing_required
        else:
            return module.options

    def _argument_auto_cast(self, arg):
        """
        Handles automatic conversion of bool and int values.
        Setting options via API commands in metasploit requires the type
        to be correct.
        """
        arg_lower = arg.lower()

        if arg_lower == "false":
            ret = False
        elif arg_lower == "true":
            ret = True
        else:
            ret = int(arg)

        return ret

    def set_module_option(self, module, option, value):
        """
        Sets a module option
        """
        try:
            module[option] = value
            Logger.log(self, f"Setting option - {module=}, {option=}, {value=}", level=Logger.INFO)
        except TypeError:
            module[option] = self._argument_auto_cast(value)
            Logger.log(self, f"Setting option - {module=}, {option=}, {value=}", level=Logger.INFO)
        # Invalid option name
        except KeyError:
            Logger.log(self, f"Invalid option name - {module=}, {option=} [ignoring...]", level=Logger.WARNING)
            pass

    def console_execute(self, command):
        """
        Executes a command on a console
        """
        self.client.consoles.console(self.cid).write(command)
        return self.console_read()

    def console_read(self):
        """
        Reads an output from a console
        """
        for i in range(self.READ_CONSOLE_BUSY_ATTEMPTS):
            if self.client.consoles.console(self.cid).is_busy():
                time.sleep(self.READ_CONSOLE_DELAY)
            else:
                return self.client.consoles.console(self.cid).read()

    def execute(self, module, payload=None):
        """
        Executes a module
        """
        if payload:
            output = self.client.consoles.console(self.cid).run_module_with_output(module, payload=payload)
            Logger.log(self, f"Executing module with payload - {module=}, {payload=}", level=Logger.INFO)
        else:
            output = self.client.consoles.console(self.cid).run_module_with_output(module)
            Logger.log(self, f"Executing module without payload - {module=}", level=Logger.INFO)

        return output

    def get_active_sessions(self, sleep=True):
        """
        Returns a list of open sessions associated with the client
        """
        if sleep:
            time.sleep(MetasploitWrapper.GET_SESSIONS_DELAY)
        if self.client:
            return copy.deepcopy(self.client.sessions.list)

