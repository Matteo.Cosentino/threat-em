import json
from time import sleep
from abc import ABC
from abc import abstractmethod

from util import time_limit


class Instruction:
    """Manages single attack instruction execution"""

    # Edit: supported_commands.json to add/remove commands

    commands_file = open("supported_commands.json", "r")
    commands_string = commands_file.read()
    commands = json.loads(commands_string)
    commands_file.close()

    core_commands = commands["core_commands"]

    module_commands = commands["module_commands"]

    exploit_commands = commands["exploit_commands"]

    meterpreter_commands = commands["meterpreter_commands"]

    # ADD HERE COMMANDS NEEDED FOR OOB ATTACKS
    misc = commands["misc"]

    supported_commands = core_commands + module_commands + exploit_commands + meterpreter_commands + misc

    def __init__(self, command, options=None):
        self.command = command
        self.options = options

        self.output = ""

    @classmethod
    def from_string(cls, instruction_string):
        """
        Loads an instruction from a resource script line/instruction
        """

        # if not empty string
        if instruction_string:
            args = instruction_string.split()
            # extract command
            command = args[0]
            # extract arguments
            options = args[1:]
            return cls(command, options)
        else:
            return None

    @classmethod
    def from_json(cls, instruction_json):
        """
        Loads an instruction from a json representation
        Unused at the moment.
        """
        if instruction_json:
            try:
                instruction_dict = json.loads(instruction_json)
                return cls(instruction_dict["command"], " ".join(instruction_dict["options"]))
            # invalid representation
            except KeyError:
                return None
        else:
            return None

    def sanity_check(self):
        """
        Checks if instruction command is valid (in supported_commands list) and if option format is correct
        """
        # supported commands are in core_commands, module_commands and exploit_commands
        if self.command in Instruction.supported_commands:
            # only valid types for options are None and list
            if isinstance(self.options, list) or self.options is None:
                return True
        else:
            return False

    def _to_dict(self):
        """
        Converts instruction to dict
        """
        if self.sanity_check():
            return {"command": self.command, "options": self.options}

    def to_string(self):
        """
        Converts instruction to string
        """
        if self.sanity_check():
            if self.options:
                return self.command + " " + " ".join(self.options)
            else:
                return self.command
        return None

    def execute(self, metasploit_client, active_module=None):
        """
        Executes instruction.
        Whenever possible, this function uses metasploit RCP API, otherwise it interacts with the console.
        This method is built so that sessions and routes opened with API or console interactions are shared.
        """

        command_dict = {"use": metasploit_client.use_module,
                        "set": metasploit_client.set_module_option,
                        "exploit": metasploit_client.execute,
                        "other": metasploit_client.console_execute}

        # handles use command
        if self.command == "use":
            # implies execute only has one option!
            # TODO: [future work] - support more flags
            # TODO: [future work] - look into -j flag for the exploit command.
            #  It might help in case some payloads open sessions not detected by the framework.
            #print(self.options[0])
            m_type, m_path = self.options[0].split("/", 1)

            if m_type and m_path:
                active_module = command_dict[self.command](m_type, m_path)
            else:
                raise InvalidCommand("The supplied command is invalid", "wrong m_type or m_path")

        # handles set command
        elif self.command == "set":
            if self.options[0:1] == ["payload"]:
                self.output = command_dict["other"](self.command + " " + " ".join(self.options))
            elif active_module:
                if self.options:
                    command_dict[self.command](active_module, self.options[0], self.options[1])
                else:
                    raise InvalidCommand("The supplied command is invalid", "no value specified")
            #else:
            #    raise InvalidCommand("The supplied command is invalid", "no module set")

        # handles exploit command
        elif self.command == "exploit":
            if self.options:
                # check if payload flag is specified
                # slicing used to avoid handling exceptions in case the option is not specified
                if self.options[0:1] == ["-p"]:
                    # check if payload is specified
                    if self.options[1:2]:
                        aus_payload = metasploit_client.use_module("payload", self.options[1])
                        # check if payload instance was created correctly
                        if aus_payload:
                            # check if there's an active module
                            if active_module:
                                self.output = command_dict[self.command](active_module, payload=aus_payload)
                                return active_module
                            else:
                                raise InvalidCommand("The supplied command is invalid", "no module set")
                        else:
                            raise InvalidCommand("The supplied command is invalid", "invalid payload")
                    else:
                        raise InvalidCommand("The supplied command is invalid", "payload not specified")
                else:
                    raise OperationNotAllowed("This operation is not supported",
                                              "the only option supported by exploit is -p followed by the exploit path")

            if active_module:
                self.output = command_dict[self.command](active_module)
            else:
                raise InvalidCommand("The supplied command is invalid", "no module set")

        # fallback option executes commands on console (not using API)
        else:
            self.output = command_dict["other"](self.command + " " + " ".join(self.options))

        return active_module


class Attack(ABC):
    """
    Abstract class defining attacks.
    """
    def __init__(self, instructions, is_non_standard=False, metasploit_client=None, ip=None, session=None, time_wait=10):
        self.instructions = instructions
        self.ip = ip
        self.session = session
        self.metasploit_client = metasploit_client
        self.is_non_standard = is_non_standard
        self.time_wait = time_wait

    @classmethod
    def from_string(cls, attack_string):
        """
        Loads an attack from a resource script
        """

        # if not empty string
        if attack_string:
            # remove \r from strings
            attack_string.replace('\r', '')
            instructions_string = attack_string.split('\n')

            attack_instructions = []
            for i in instructions_string:
                if i:
                    i_x = Instruction.from_string(i)
                    attack_instructions.append(i_x)

            return cls(attack_instructions)
        else:
            return None

    @classmethod
    def from_json(cls, attack_json):
        """
         Loads an attack from a json representation
         """
        #print(f"{attack_json=}")
        if attack_json:
            try:
                attack_dict = json.loads(attack_json)

                attack_instructions = []

                #{"attack_name": [{"command": "foommand", "options": ["bar", "spam"]},
                #                 {"command": "barmand", "options": ["foo", "spam"]}]}

                k = list(attack_dict.keys())[0]
                #print(f"{k=}")
                #print(f"{attack_dict[k]=}")

                for x in attack_dict[k]:
                    i_k = Instruction.from_json(json.dumps(x))
                    #print(i_k.command, i_k.options)
                    attack_instructions.append(i_k)

                return cls(attack_instructions)
            # invalid representation
            except KeyError:
                return None
        else:
            return None

    def sanity_check(self):
        """
        Checks if every instruction of the attack is valid.
        """
        for i in self.instructions:
            if not isinstance(i, Instruction):
                return False
            if not i.sanity_check():
                return False
        return True

    def to_json(self):
        """
        Converts instruction to JSON format
        """
        if self.sanity_check():
            attack = []
            cont = 0

            for i in self.instructions:
                attack.append(i._to_dict())
                cont += 1

            return json.dumps({"attack": attack})

    def to_string(self):
        """
        Converts instruction to string
        """
        if self.sanity_check():
            result = ""
            for i in self.instructions:
                result += i.to_string() + "\r\n"
            return result

    def execute(self):
        raise NotImplementedError("Use specific attacks!")

    def check(self):
        raise NotImplementedError("Use specific attacks!")


class IBAttack(Attack):
    """
    In-band attacks implementation
    """
    def __init__(self, instructions, metasploit_client, ip, time_wait=10, is_non_standard=False):
        super().__init__(instructions, is_non_standard, metasploit_client, ip, time_wait=time_wait)

    def execute(self):
        """
        Executes each instruction of an IB-attack
        """
        curr_module = None
        output_trace = []
        for i in self.instructions:
            curr_module = i.execute(self.metasploit_client, curr_module)
            output_trace.append(i.output)

        return output_trace

    def check(self):
        """
        NOT IMPLEMENTED AND NOT USED: it should check if a session is opened after the attack.
        TODO: move checks here!
        """
        return None


class IBAttackOnConsole(IBAttack):
    """
    Non-standard In-band attacks implementation through console interaction.
    For attacks that won't work properly in a standard execution context.
    """

    # Check time_wait not used

    SLEEP_TIME = 10
    SHORT_SLEEP_TIME = 0.5

    def __init__(self, instructions, metasploit_client, ip, time_wait=10):
        super().__init__(instructions, metasploit_client, ip, time_wait=time_wait, is_non_standard=True)
        self.output_trace = None
        self.is_non_standard = True
        self.ip = ip

    def execute(self):
        """
        Executes each instruction of an attack on a console.
        Sleeps after each instruction to be able to capture possible outputs.
        """
        self.output_trace = []
        cid = self.metasploit_client.client.consoles.console().cid
        # flush
        _ = self.metasploit_client.client.consoles.console(cid).read()

        for i in self.instructions:
            x = i.to_string()

            if x:
                self.metasploit_client.client.consoles.console(cid).write(x)
                if "exploit" in x or "run" in x or "resource" in x:
                    # After exploit, run or resource commands
                    # it may take a while to show the output depending on the attack
                    # the sleep time should be adjusted to avoid false negative and to avoid wasting time
                    sleep(IBAttackOnConsole.SLEEP_TIME)
                else:
                    # sleep time for ordinary commands
                    sleep(IBAttackOnConsole.SHORT_SLEEP_TIME)
                out = self.metasploit_client.client.consoles.console(cid).read()
                self.output_trace.append(out["data"])
                #print(out["data"])

        # sometimes the console hangs and it's impossible to stop the execution of an attack
        # while the console it's busy it's impossible to do anything else than wait
        # after 300 seconds, an exception will be raised.
        with time_limit(300):
            while self.metasploit_client.client.consoles.console(cid).is_busy():
                sleep(1)

        out = self.metasploit_client.client.consoles.console(cid).read()
        self.output_trace.append(out["data"])

        self.output_trace = self.output_trace
        return self.output_trace

    def check(self):
        """
        Checks if the attack opened a session
        """

        # This keyword always worked for metasploit sessions,
        # it might need to be changed if some attack uses it
        # even in case no new session is opened.
        session_opened_trace = "opened"
        flag = False

        for r in self.output_trace:
            if r.find(session_opened_trace) > -1:
                flag = True

        return flag


class OOBAttack(Attack):
    """
    Abstract class defining Out-Of-Band attacks.
    """
    def __init__(self, instructions, ip=None, session=None, time_wait=10):
        super().__init__(instructions, True, ip, session, time_wait=time_wait)
        self.out = []

    @abstractmethod
    def check(self):
        pass

    @abstractmethod
    def execute(self):
        pass


class OOBShellInteractionWithExitCode(OOBAttack):
    """
    SSH Login OOB attack implementation through console interaction.
    For attacks that won't work properly in a standard execution context.
    """
    SLEEP_TIME = 5

    def __init__(self, instructions, ip, session, time_wait=None):
        # the execute function waits SLEEP_TIME seconds for each command executed
        # 10 seconds are just to consider delays
        time_wait = (len(instructions) * OOBShellInteractionWithExitCode.SLEEP_TIME) + 10
        super().__init__(instructions, ip, session, time_wait=time_wait)
        self.out = []

        self.instructions = instructions
        self.session = session
        self.ip = ip
        self.is_non_standard = True

        if type(self.session) == str:
            raise TypeError

    def check(self):
        """
        Checks if the attack succesfully logged in.
        The check is done on the exit status of the ssh connection command.
        """
        # Checks if exit code of ssh login is 0 (succesful login)
        if self.out[-1:] == ['0\n']:
            return True
        else:
            return False

    def execute(self):
        """
        Executes each instruction of an attack on a console.
        Sleeps after each instruction to be able to capture possible outputs.
        """
        # Run a shell command within a meterpreter session

        for c in self.instructions:
            x = c.to_string()
            if x:
                self.session.write(x)
                sleep(OOBShellInteractionWithExitCode.SLEEP_TIME)
                y = self.session.read()
                self.out.append(y)

        return self.out


class InvalidCommand(Exception):
    """
    Exception for invalid commands
    """
    def __init__(self, message, errors):
        super().__init__(message)
        self.errors = errors
        print(errors)


class OperationNotAllowed(Exception):
    """
    Exception for invalid operations
    """
    def __init__(self, message, errors):
        super().__init__(message)
        self.errors = errors
        print(errors)