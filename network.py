import ipaddress
import random
import docker

from util import Constants


class IPLookup:
    """
    Class that manage the lookup of IP addresses in different networks
    """
    def __init__(self):
        self.addresses = IPLookup._update_backdoor_aliases()

    @staticmethod
    def _update_backdoor_aliases():
        # Connects to docker daemon
        client = docker.DockerClient()

        # Get containers
        containers = client.containers.list()

        addresses = {}

        # For each container
        for c in containers:
            # Get active networks
            networks = c.attrs['NetworkSettings']['Networks']

            true_net_ips = []
            backdoor_ip = None

            # For each network
            for n in networks:
                # if it's the backdoor network, get the lookup dict key
                if n == Constants.BACKDOOR_NETWORK_NAME:
                    try:
                        backdoor_ip = networks[n]['IPAddress']
                    except Exception as e:
                        print(e)

                # if it's not the backdoor network get the "true network" IPs
                else:
                    try:
                        true_net_ips.append(networks[n]['IPAddress'])
                    except Exception as e:
                        print(e)

            # Only include containers in the backdoor network
            if backdoor_ip:
                if true_net_ips:
                    addresses[true_net_ips[0]] = [backdoor_ip]
                else:
                    # In case the container only uses the backdoor network
                    addresses[backdoor_ip] = [backdoor_ip]

        return addresses

    def get_ip_backdoor_alias(self, ip):
        """
        Gets the backdoor network IP given an IP.
        """
        return self.addresses.get(ip)

    @staticmethod
    def get_ip_aliases(ip):
        """
        Get every IP of a machine given one of its IPs.
        """
        # Connects to docker daemon
        client = docker.DockerClient()

        # Get containers
        containers = client.containers.list()

        # For each container
        for c in containers:
            # Get active networks
            networks = c.attrs['NetworkSettings']['Networks']

            # For each network
            for n in networks:
                # If we found the ip, return network
                try:
                    ip_found = networks[n]['IPAddress']
                    if ip_found == ip:
                        return {networks[n]['IPAddress'] for n in networks}
                except Exception as e:
                    print(e)

    @staticmethod
    def get_network_from_ip(ip, prefix_len=True):
        """
        Gets a network given a machine IP
        """
        # Connects to docker daemon
        client = docker.DockerClient()

        # Get containers
        containers = client.containers.list()

        # For each container
        for c in containers:
            # Get active networks
            networks = c.attrs['NetworkSettings']['Networks']

            # For each network
            for n in networks:
                # If we found the ip, return network
                try:
                    ip_found = networks[n]['IPAddress']
                    if ip_found == ip:
                        # gets the IP address and appends the network prefix length
                        ip_and_subnet = ip_found + "/" + str(networks[n]['IPPrefixLen'])

                        # returns the network IP address
                        if prefix_len:
                            return ipaddress.IPv4Network(ip_and_subnet, False).with_prefixlen
                        else:
                            return str(ipaddress.IPv4Network(ip_and_subnet, False))

                except Exception as e:
                    print(e)

    @staticmethod
    def get_containers_in_network(network_ip):
        """
        Get every container in a given network.
        """
        # Connects to docker daemon
        client = docker.DockerClient()

        # Get containers
        containers = client.containers.list()

        container_ips = []
        # For each container
        for c in containers:
            # Get active networks
            networks = c.attrs['NetworkSettings']['Networks']

            # For each network
            for n in networks:
                try:
                    ip_to_check = ipaddress.IPv4Network(networks[n]["Gateway"] + "/" + str(networks[n]['IPPrefixLen']),
                                                        False).with_prefixlen

                    if ip_to_check == network_ip:
                        try:
                            container_ips.append(networks[n]['IPAddress'])

                        except Exception as e:
                            print(e)
                except Exception as e:
                    pass

        return container_ips


class PortPool:
    """
    Class to manage free ports to use for backdoor purposes.
    Ports are used only once, then "discarded".
    """

    START_PORT = 10000
    END_PORT = 11000

    def __init__(self):
        self.free_ports = set(range(PortPool.START_PORT, PortPool.END_PORT))
        self.used_ports = set()

    def update_used_ports(self):
        self.free_ports = self.free_ports - self.used_ports

    def get_free_port(self):
        to_use = random.choice(tuple(self.free_ports))
        self.used_ports.add(to_use)
        self.update_used_ports()

        return to_use

    def get_used_ports(self):
        return self.used_ports
