import time
import logging
import inspect
import ipaddress
import json

from contextlib import contextmanager
import sys
import os

import signal


# TIMEOUT MANAGEMENT
class TimeoutException(Exception):
    pass


@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException("Timed out!")

    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)


class SuppressErr:
    @staticmethod
    @contextmanager
    def suppress_stderr():
        with open(os.devnull, "w") as devnull:
            old_stderr = sys.stderr
            sys.stderr = devnull
            try:
                yield
            finally:
                sys.stderr = old_stderr


class Constants:
    LOG_FILE_NAME = str(time.time()).split(".")[0] + "_report.log"
    LOG_FILE_FORMAT = '[%(asctime)-15s] - %(module)s_%(class)s: %(message)s'
    LOG_FILE_DIR = 'logs/'

    BACKDOOR_NETWORK_NAME = "bridge"  # "backdoor_network"
    BACKDOOR_NETWORK = ipaddress.IPv4Network("172.17.0.0/16", False)

    COL_GREEN = '\033[92m'  # GREEN
    COL_YELLOW = '\033[93m'  # YELLOW
    COL_RED = '\033[91m'  # RED
    COL_RESET = '\033[0m'  # RESET COLOR

    """
    When generating a naive attack, a function iterates through modules randomly
    trying to set RHOSTS and LHOST. If it's not possible to set these values, 
    the function just loops. For this reason it's important to use loose criteria,
    especially when generating a few naive attacks.
    """

    # TODO: [future work] - differentiate search criteria depending on the attack step

    db_file = open("attack_db.json", "r")
    db_string = db_file.read()
    db = json.loads(db_string)
    db_file.close()

    NAIVE_ATTACKS_SEARCH_CRITERIA = db["naive_attacks_search_criteria"]


class Logger:
    # https://docs.python.org/3/library/logging.html#levels
    INFO = 20  # logging.info
    DEBUG = 10  # logging.debug
    WARNING = 30  # logging.warning
    ERROR = 20  # logging.error
    NOTSET = 0

    @staticmethod
    def init_logger():
        logging.basicConfig(filename=Constants.LOG_FILE_DIR + Constants.LOG_FILE_NAME, level=20)
        logging.basicConfig(format=Constants.LOG_FILE_FORMAT)

    @staticmethod
    def log(cls, message, level=logging.DEBUG):
        cls_name = cls.__class__.__name__
        mod_name = inspect.getfile(cls.__class__)
        # extra = {'module': mod_name, 'class': cls_name}
        logging.log(level, message)  # extra=extra)

    @staticmethod
    def turn_off(module):
        logging.getLogger(module).setLevel(Logger.NOTSET)

